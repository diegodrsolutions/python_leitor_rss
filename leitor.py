# Importando as bibliotecas necessárias
import feedparser
import re
from time import mktime
from datetime import datetime, timedelta

# Opções de RSS disponíveis
opcoes = [['G1 - Todas as notícias', 'http://g1.globo.com/dynamo/rss2.xml'],
          ['Concursos e Emprego', 'http://g1.globo.com/dynamo/concursos-e-emprego/rss2.xml'],
          ['Planeta Bizarro','http://g1.globo.com/dynamo/planeta-bizarro/rss2.xml'],
          ['Tecnologia e Games', 'http://g1.globo.com/dynamo/tecnologia/rss2.xml'],
          ['Turismo e Viagem', 'http://g1.globo.com/dynamo/turismo-e-viagem/rss2.xml'],
          ['Ciência/Tecnologia','http://news.google.com/news?cf=all&hl=pt-BR&pz=1&ned=pt-BR_br&topic=t&output=rss'],
          ['Entretenimento','http://news.google.com/news?cf=all&hl=pt-BR&pz=1&ned=pt-BR_br&topic=e&output=rss'],
          ['Paulínia','http://news.google.com/news?cf=all&hl=pt-BR&pz=1&ned=pt-BR_br&geo=detect_metro_area&output=rss']
          #['',''],
         ]
print('Qual tipo de notícia você deseja?\n')
i=1
for linha in opcoes:
    print(i, '-', linha[0])
    i += 1
print()

# Validando se o usuário escolheu a opção correta
escolha = input('> ')

try:
    if len(escolha)<=0:
        exit()
    elif int(escolha)<=0 or int(escolha)>len(opcoes):
        exit()
except:
    exit()
print()

# Buscando os feeds
d = feedparser.parse(opcoes[int(escolha)-1][1])
lista_de_feeds = d.entries

# Listando os feeds do RSS
for feed in lista_de_feeds:
    # Titulo
    if 'title' in feed:
        titulo = feed.title
        print('Título........:', titulo)

    # Resumo
    if 'summary' in feed:
        sumario = re.sub("  ", " ", feed.summary)
        sumario = re.sub("\n", " ", sumario)
        sumario = re.sub("<.*?>", "", sumario)
        #print('Sumário.......:', sumario)

    # Link da noticia
    if 'link' in feed:
        link = feed.link
        print('Link..........:', link)

    # Descrição
    if 'description' in feed:
        #print('Description...:', feed.description)
        descricao = re.sub("  ", " ", feed.description)
        descricao = re.sub("\n", " ", descricao)
        descricao = re.sub("<.*?>", "", descricao)
        print('Descrição.....:', descricao)

    # Categoria
    if 'category' in feed:
        categoria = feed.category
        print('Categoria.....:', categoria)

    # Data e hora
    data_hora = dt = datetime.fromtimestamp(mktime(feed.updated_parsed))
    data = data_hora - timedelta(hours=3)
    data_e_hora = data.strftime('%d/%m/%Y %H:%M:%S')
    print('Data e hora...:', data_e_hora)

    # Pulando uma linha entre cada feed
    print()